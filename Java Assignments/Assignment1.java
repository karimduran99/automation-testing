import java.util.Scanner;

public class Assignment1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Write hot:");
        String str1 = in.nextLine();
        System.out.println("Write dog:");
        String str2 = in.nextLine();
        System.out.println(str1 + str2);
//        System.out.println("Choose 3 numbers");
//        int x = in.nextInt();
//        int y = in.nextInt();
//        int z = in.nextInt();
        System.out.println(getGreatestNum(23,12,19));
        System.out.println(getGreatestNum(122,98,377));
//        System.out.println("Choose a count for fibonacci series");
//        int count = in.nextInt();
        printFibonacciNum(8);
        System.out.println(" ");
        printFibonacciNum(25);
        System.out.println(" ");
//        System.out.println(printFibonacciNum(8));
//        System.out.println(printFibonacciNum(25));
        System.out.println("Choose 4 numbers");
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        int w = in.nextInt();
        System.out.println(getGreatestNum(a,b,c,w));
    }

    public static void printFibonacciNum(int count){
        int n = 0;
        int n1 = 1;
        for (int i = 0; i < count; i++) {
            int n2 = n+n1;
            n = n1;
            n1 = n2;
            System.out.print(n2 + ", ");
        }
    }
    public static int getGreatestNum(int x, int y, int z){
        int num = Math.max(x,Math.max(y,z));
        return num;
    }
    public static int getGreatestNum(int x, int y, int z, int w){
        int num = Math.max(Math.max(x,w),Math.max(y,z));
        return num;
    }
}
