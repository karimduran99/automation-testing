public class Compare {
    String brand;
    int price;
    public Compare(String brand, int price){
        this.brand = brand;
        this.price = price;
    }
    @Override public boolean equals(Object obj){
        if (this == obj){
            return true;
        }
        if (this.getClass() != obj.getClass()){
            return false;
        }
        Compare k1 = (Compare) obj;
        return this.brand.equals(k1.brand) && this.price == k1.price;
    }
}
