public class DogOverloading{
    public void bark(){
        System.out.println("woof");
    }
    public void bark(int num){
        for (int i=0;i<num;i++){
            System.out.println("woofs");
        }
    }
    public static void main(String[] args) {
        DogOverloading sound = new DogOverloading();
        sound.bark(5);
        sound.bark();
    }
}
