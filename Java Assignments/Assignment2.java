import java.util.Scanner;

public class Assignment2 {
    public static void main(String[] args){
        getFibNum(5);
        System.out.println("Choose 2 numbers");
        Scanner in = new Scanner(System.in);
        double num1 = in.nextInt();
        double num2 = in.nextInt();
        excHandlingNumber(num1,num2);
        System.out.println("Enter your name");
        Scanner inp = new Scanner(System.in);
        String foreName = inp.nextLine();
        String midName = null;
        excHandlingString(foreName, midName);
        studentsIND();
    }
    public static int i = 1;
    public static int n1 = 0;
    public static int n2 = 1;
    public static void getFibNum(int count){
        int n3 = n1+n2;
        n1 = n2;
        n2 = n3;
        System.out.println(n3);
        if (i<count) {
            getFibNum(count-1);
        }
    }
    public static void excHandlingNumber(double num1, double num2){
        try{
            double result = num1/num2;
            System.out.println("Division result: " + result);

        }
        catch (ArithmeticException e){
            System.out.println("Cannot resolve, result = infinity");
        }
    }
    public static void excHandlingString(String foreName, String midName){
        try{
            System.out.println(foreName.concat(midName));
        }
        catch (NullPointerException e){
            System.out.println("Cannot resolve, Middle Name = null");
        }
    }
    public static void studentsIND(){
        for (int j = 0; j < 3; j++) {
            int sid = 0;
            Scanner inp = new Scanner(System.in);
            System.out.println("Enter Student name");
            System.out.print("Fore Name: ");
            String namefore = inp.nextLine();
            System.out.print("Middle Name: ");
            String namemid = inp.nextLine();
            System.out.print("Last Name: ");
            String namelast = inp.nextLine();
            Student student = new Student(namefore,namemid,namelast);
            student.getStudentName();
            System.out.println("Enter Student DOB || (dd/mm/yyyy)");
            Scanner inpu = new Scanner(System.in);
            String date = inpu.nextLine();
            Student dobdate = new Student(date);
            dobdate.setStudentDOB();
            Student id = new Student(sid);
            id.getStudentID();
        }
    }
}
