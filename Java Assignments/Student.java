import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Student {
    private int StudentId;
    private String FirstName;
    private String MiddleName;
    private String LastName;
    private String DOB;
    public Student(String foreName,String midName, String lastName){
        this.FirstName = foreName;
        this.MiddleName = midName;
        this.LastName = lastName;
    }
    public Student(String dob){
        this.DOB = dob;
    }
    public Student(int id){
        this.StudentId = id;
    }
    public void getStudentName(){
        String fullName = FirstName + " " + MiddleName + " " + LastName;
        System.out.println(fullName);
    }
    public void setStudentDOB(){
        String oldstring = DOB + " 00:00:00.0" ;
        LocalDateTime datetime = LocalDateTime.parse(oldstring, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.S"));
        String newstring = datetime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        this.DOB = newstring;
        System.out.println(DOB);
    }
    public void getStudentID() {
        this.StudentId = (int) (Math.random() * (1000 - 1)) + 1;
        System.out.println("New student ID: " + StudentId);
    }
}
