public class DogOverriding{
    public void bark(){
        System.out.println("woof");
    }
}
class hound extends Dog{
    public void sniff(){
        System.out.println("snifffff");
    }
    public void bark(){
        System.out.println("bowl");
    }

    public static void main(String[] args) {
        hound sound = new hound();
        sound.bark();
        sound.sniff();
        DogOverriding barking = new DogOverriding();
        barking.bark();
    }
}
