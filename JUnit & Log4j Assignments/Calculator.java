public class Calculator {
    public static int sum(int n1, int n2){
        return n1+n2;
    }
    public static int diff(int n1, int n2){
        return n1-n2;
    }
    public static int divide(int n1, int n2){
        return n1/n2;
    }
    public static int multiply(int n1, int n2){
        return n1*n2;
    }
}
