import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
@RunWith(Parameterized.class)
public class CalculatorTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, 1, 1},
                {2, 2, 4},
                {3, 2, 6}
        });
    }
    private final Calculator op;
    private final int n1;
    private final int n2;
    private final int result;
    public CalculatorTest(int n1, int n2, int result){
        this.op = new Calculator();
        this.n1 = n1;
        this.n2 = n2;
        this.result = result;
    }
    @Test (expected = ArithmeticException.class)
    public void testdiv(){
        int n1 = 5; int n2 = 0; int ndiv = Math.floorDiv(n1,n2);
        Assert.assertEquals(ndiv,Calculator.divide(n1,n2));
    }
    @Test
    public void testsum(){
        int n1 = 5; int n2 = 0; int nsum = Math.addExact(n1,n2);
        Assert.assertEquals(nsum,Calculator.sum(n1,n2));
    }
    @Test
    public void testdiff(){
        int n1 = 5; int n2 = 0; int ndiff = Math.subtractExact(n1,n2);
        Assert.assertEquals(ndiff,Calculator.diff(n1,n2));
    }
    @Test
    public void testmult(){
        Assert.assertEquals(result,op.multiply(n1,n2));
    }
}