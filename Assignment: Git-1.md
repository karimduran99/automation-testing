1. __Define *branch* in GitLab. What is purpose of master. Explain with example.__  
A **branch** is a different line created to develop or modify a project/document. The *master* branch is the main branch were the original text or  
doc is, and the changes expected are merged if necessary, it helps to maintain a better organization. i.e. When you want to make changes in any  
code, you can create a new branch that may help you to remark the differences between the original code and the new one, and, if it is helpful,  
then the new branch merges into the original one.
2. __Explain the git command with example: Init, Push, Status, Commit__
* git init: This command is used when a new repository is desired. When executed, it creates a new subdirectory as ".git" and a master branch.  
i.e. When you want to add a file from your computer to your GitLab account, you select the folder from where you want to extract the file and the  
folder must have the repository attribute, that's when git init is used, to use the folder as a repository.
* git push: This command is used to load local changes to the remote repository selected. i.e. To add changes or new files to the project from  
your repository they must be pushed into it.
* git status: This command shows the state of the selected repository and staging area, allowing us to see the untracked or tracked ("git add file  
command") files. i.e. Before submitting the commit command, check if the file is tracked with the status command and see if you must continue or  
make some changes.
* git commit: This command saves all staged changes, moving files from staging area to commit.
3. __What is the difference between staging and commiting in GitLab. Explain with example.__
Staging is the step where you make the all the changes you want to add to the files, and the commit is the final step where you save all the  
changes from stage and commit the files to the local repository. i.e. If you want to make changes to a file, first the file must be in the staging  
area, where all the changes will be applied to it, and then saved and committed to the local repository by the commit command.
