package Tutorial;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import java.util.concurrent.TimeUnit;

public class SeleniumTutorial {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\Browsers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
//        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        driver.get("https://formy-project.herokuapp.com/");
//        Actions action = new Actions(driver);
//        WebElement search = driver.findElement(By.xpath("//a[text()='Help']"));
//        action.moveToElement(search);
//        search.click();

//        driver.get("https://formy-project.herokuapp.com/checkbox");
//        WebElement click = driver.findElement(By.id("checkbox-1"));
//        click.click();

//        driver.get("https://formy-project.herokuapp.com/keypress");
//        WebElement name = driver.findElement(By.id("name"));
//        name.sendKeys("Karim Emiliano Duran Ayala");

//        driver.get("https://www.google.com");
//        WebElement search = driver.findElement(By.name("q"));
//        search.sendKeys("Selenium", Keys.ENTER);

//        driver.get("https://formy-project.herokuapp.com/dragdrop");
//        WebElement drag = driver.findElement(By.id("image"));
//        WebElement drop = driver.findElement(By.id("box"));
//        Actions ac = new Actions(driver);
//        ac.dragAndDrop(drag,drop).build().perform();

//        driver.get("https://formy-project.herokuapp.com/switch-window");
//        WebElement window = driver.findElement(By.id("alert-button"));
//        window.click();
//        Alert alert = driver.switchTo().alert();
//        alert.accept();

        driver.get("https://formy-project.herokuapp.com/switch-window");
        WebElement window = driver.findElement(By.id("new-tab-button"));
        window.click();
        for (String windowTab : driver.getWindowHandles()) {
            driver.switchTo().window(windowTab);
        }
    }
}
